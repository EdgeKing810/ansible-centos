# CentOS Ansible Test Image

CentOS Docker containers for Ansible playbook and role testing.

## How to Use

  1. [Install Docker](https://docs.docker.com/engine/installation/).
  2. Pull this image from Ninux Gitlab Docker Registry:
  
  ```bash
    # CentOS 6
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-centos:6
    
    #CentOS 7
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-centos:7
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-centos:latest
  ```
   
  3. Run a container from the image:
    
  ```bash
    # CentOS 6
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-centos:6 /sbin/init
    
    # CentOS 7
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-centos:7 /usr/lib/systemd/systemd
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-centos:latest /usr/lib/systemd/systemd
  ```

  4. Use Ansible inside the container:
    
  ```bash
    docker exec --tty [container_id] env TERM=xterm ansible --version
    docker exec --tty [container_id] env TERM=xterm ansible-playbook /path/to/ansible/playbook.yml --syntax-check
  ```
    